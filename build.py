#!/usr/bin/env python3

import os
import glob
import subprocess
import signal
import argparse
import sys
import termios

toolchain_path = os.path.expanduser('~/opt/cross/bin')
toolchain_target = "i686-elf"
cxx_flags = "-std=c++17 -ffreestanding -Wall -Wextra -fno-exceptions -fno-rtti -g".split()
ld_flags = "-ffreestanding -nostdlib".split()

qemu_path = "qemu-system-i386"
qemu_args = []

def tool_path(tool):
    return f"{toolchain_path}/{toolchain_target}-{tool}"

def run_cmd(args):
    # So we can restore old termios settings after a ctrl-c interrupt
    fd = sys.stdin.fileno()
    old = termios.tcgetattr(fd)

    def preexec():
        # Pressing ctrl-c in a gdb session was causing all hell to break loose
        # https://stackoverflow.com/a/5050521
        signal.signal(signal.SIGINT, signal.SIG_IGN)

    print(" ".join(args))
    try:
        subprocess.run(args, preexec_fn=preexec)
    except KeyboardInterrupt:
        termios.tcsetattr(fd, termios.TCSADRAIN, old)

def object_file_path(src_path):
    first, rest = src_path.split(os.path.sep, 1)
    if first == "src":
        first = "dest"
    nonext, ext = rest.rsplit(".", 1)
    ext = "o"
    return os.path.join(first, f"{nonext}.{ext}")

def newest_modification_time(paths):
    def modtime(path):
        try:
            return os.path.getmtime(path)
        except OSError:
            return 0

    modtimes = map(modtime, paths)
    return max(modtimes)

def parse_makefile_deps(output_as_bytes):
    out = output_as_bytes.decode('utf-8').replace("\\\n", "").split(": ", 2)
    return out[1].split()

def asm_source_dependencies(src):
    return parse_makefile_deps(subprocess.check_output(["nasm", "-M", src]))

def should_assemble(src, dest):
    deps = asm_source_dependencies(src)
    src_time = newest_modification_time(deps)
    dest_time = newest_modification_time([dest])

    return src_time > dest_time

def cpp_source_dependencies(src):
    return parse_makefile_deps(subprocess.check_output([tool_path("g++"), "-M", src, *cxx_flags]))

def should_compile_cpp(src, dest):
    deps = cpp_source_dependencies(src)
    src_time = newest_modification_time(deps)
    dest_time = newest_modification_time([dest])

    return src_time > dest_time

def assemble(srcs):
    as_path = "nasm"
    objs = []
    for src in srcs:
        dest = object_file_path(src)
        if should_assemble(src, dest):
            run_cmd([as_path, src, "-o", dest, "-felf"])
            # TODO: Fail for non-zero return code

        objs.append(dest)
    return objs

def compile_cpp(srcs):
    cxx_path = tool_path("g++")
    objs = []
    for src in srcs:
        dest = object_file_path(src)
        if should_compile_cpp(src, dest):
            run_cmd([cxx_path, "-c", src, "-o", dest, *cxx_flags])

        objs.append(dest)
    return objs

def link(objs):
    dest = "kernel"

    src_time = newest_modification_time(objs)
    dest_time = newest_modification_time([dest])

    if src_time > dest_time:
        cc_path = tool_path("gcc")
        run_cmd([cc_path, "-T", "linker.ld", "-o", dest, *objs, *ld_flags, "-lgcc"])

    return dest

def run_qemu(kern, serial=True, debug=False):
    args = qemu_args.copy()
    if debug:
        args.append("-s")
        args.append("-S")

    if serial:
        args.append("-serial")
        args.append("stdio")

    proc = subprocess.Popen([qemu_path, "-kernel", kern, *args])
    if not debug:
        proc.wait()

def run_gdb():
    run_cmd(["gdb", "kernel", "-ex", "target remote localhost:1234"])

parser = argparse.ArgumentParser()
parser.add_argument('-g', '--debug', action="store_true")
parser.add_argument('--run', action="store_true")
args = parser.parse_args()

os.makedirs("dest", exist_ok = True)
objs = []
objs += assemble(glob.glob("src/**.s"))
objs += compile_cpp(glob.glob("src/**.cpp"))
kernel = link(objs)

if args.debug:
    run_qemu(kernel, debug=True, serial=False)
    run_gdb()
elif args.run:
    run_qemu(kernel)