#pragma once
#include <stdint.h>

class StringBuffer
{
public:
    template<uint32_t S>
    static StringBuffer from_static_array(char (&buffer)[S])
    {
        return StringBuffer { buffer, S-1 };
    }

    static StringBuffer from_allocation(char* buffer, uint32_t capacity)
    {
        return StringBuffer { buffer, capacity };
    }

    ~StringBuffer();

    uint32_t capacity() const { return m_capacity; }
    uint32_t length() const;
    char* unsafe_end_pointer();
    char const* start_pointer() const { return m_buffer; }

    void append(char ch);
    void clear();
private:
    StringBuffer(char* buffer, uint32_t capacity)
        : m_buffer(buffer), m_capacity(capacity)
        {
            clear();
        }

    // TODO: Make this growable
    char* m_buffer;
    uint32_t m_capacity;
};
