#pragma once
#include <stdint.h>
#include "SerialPort.h"

#define DO_STRINGIFY(x) #x
#define STRINGIFY(x) DO_STRINGIFY(x)
#define ASSERT(expr) \
    do { \
        if(!(expr)) panic("Assertion Failed: ", #expr, "\n\t", "at ", __FILE__, ":", STRINGIFY(__LINE__)); \
    } while(0)

void panic();

template<typename T, typename... Ts>
void panic(T msg, Ts... ts)
{
    SerialPort::com1.puts(msg);
    panic(ts...);
}
