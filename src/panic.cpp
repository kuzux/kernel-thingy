#include "panic.h"

void panic()
{
    SerialPort::com1.puts("\nKernel Panic");
    for(;;);
}