#pragma once
#include <stdint.h>

class Bitset
{
public:
    // TODO: Have another ctor that allocates
    Bitset(uint32_t* data, uint32_t length);

    bool test(uint32_t index) const;
    void set(uint32_t index);
    void clear(uint32_t index);

    uint32_t find_set_bit();
    uint32_t find_cleared_bit();
private:
    uint32_t find_bit(uint8_t bit_value);

    uint32_t* m_data;
    uint32_t m_length;
};
