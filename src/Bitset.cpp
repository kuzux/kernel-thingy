#include "Bitset.h"
#include "ByteOperations.h"
#include "panic.h"

#define INDEX_FROM_BIT(a) ((a)/32)
#define OFFSET_FROM_BIT(a) ((a)%32)

Bitset::Bitset(uint32_t* data, uint32_t length) 
    : m_data(data), m_length(length)
    {
        auto length_in_elems = length / 32;
        ASSERT(length % 32 == 0);
        ByteOperations::clear(m_data, length_in_elems*4);
    }

bool Bitset::test(uint32_t index) const
{
    ASSERT(index < m_length);
    auto elem_idx = INDEX_FROM_BIT(index);
    auto mask = (0x1 << OFFSET_FROM_BIT(index));
    return m_data[elem_idx] & mask;
}

void Bitset::set(uint32_t index)
{
    ASSERT(index < m_length);
    auto elem_idx = INDEX_FROM_BIT(index);
    auto mask = (0x1 << OFFSET_FROM_BIT(index));
    m_data[elem_idx] |= mask;
}

void Bitset::clear(uint32_t index)
{
    ASSERT(index < m_length);
    auto elem_idx = INDEX_FROM_BIT(index);
    auto mask = (0x1 << OFFSET_FROM_BIT(index));
    m_data[elem_idx] &= ~mask;
}

uint32_t Bitset::find_bit(uint8_t bit_value)
{
    uint32_t all_bits_mask = (bit_value == 0x0) ? 0xffffffff : 0x00000000;

    auto length_in_elems = m_length / 32;

    for(uint32_t i=0; i<length_in_elems; i++) {
        if(m_data[i] == all_bits_mask) continue;

        for(uint32_t j=0; j<32; j++) {
            auto mask = 1 << j;
            if((m_data[i] & mask) == bit_value) return 32*i + j;
        }
    }

    return m_length;
}

uint32_t Bitset::find_set_bit()
{
    return find_bit(0x1);
}

uint32_t Bitset::find_cleared_bit()
{
    return find_bit(0x0);
}
