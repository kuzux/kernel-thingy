#include "MemoryManager.h"
#include "Bitset.h"
#include "EternalHeap.h"
#include "idt.h"
#include "panic.h"
#include "ByteOperations.h"

#include "StringBuffer.h"
#include "Formatter.h"

namespace MemoryManager
{
struct __attribute__((packed)) Page
{
    uint8_t present    : 1;    // Page present in memory
    uint8_t rw         : 1;    // Read-only if clear, readwrite if set
    uint8_t user       : 1;    // Supervisor level only if clear
    uint8_t accessed   : 1;    // Has the page been accessed since last refresh?
    uint8_t dirty      : 1;    // Has the page been written to since last refresh?
    uint8_t unused     : 7;    // Amalgamation of unused and reserved bits
    uint32_t frame     : 20;   // Frame address (shifted right 12 bits)
};

struct __attribute__((packed)) PageTable
{
    Page pages[1024];
};

struct __attribute__((packed)) PageDirectory
{
    PageTable* tables[1024];
    // NOTE: We either need to keep these values cached inside the structure
    //       or recompute them every time we need to switch page tables
    //       since we need the physical addresses
    uint32_t tables_physical[1024];
    // We also need to load the physical address of the tables_physical array
    uint32_t physical_address;
};

static PageDirectory* s_directory;

PageDirectory* kernel_directory()
{
    return s_directory;
}

void switch_directory(PageDirectory* directory)
{
    asm volatile("mov %0, %%cr3" ::"r"(directory->physical_address));

    uint32_t cr0;
    asm volatile("mov %%cr0, %0" :"=r"(cr0));
    cr0 |= 0x80000000;
    asm volatile("mov %0, %%cr0" ::"r"(cr0));
}

static void fill_page_table(uint16_t page_table_idx)
{
    s_directory->tables[page_table_idx] = EternalHeap::allocate<PageTable>(1, 0x1000);
    s_directory->tables_physical[page_table_idx] = (uint32_t)s_directory->tables[page_table_idx];
    s_directory->tables_physical[page_table_idx] |= 0x7; // present, rw, us

    for(uint16_t idx_in_table=0; idx_in_table<1024; idx_in_table++) {
        auto& page = s_directory->tables[page_table_idx]->pages[idx_in_table];
        ByteOperations::fill(&page, 0, sizeof(Page));
        page.present = 1;
        page.rw = 0;
        page.user = 1;

        uint32_t frame_addr = (page_table_idx << 10) | idx_in_table;
        page.frame = frame_addr;
    }
}

void setup(uint16_t tables_to_fill)
{
    ASSERT(tables_to_fill <= 1024);

    // Just identity map the entire memory
    s_directory = EternalHeap::allocate<PageDirectory>(1, 0x1000);
    for(uint16_t page_table_idx=0; page_table_idx<tables_to_fill; page_table_idx++)
        fill_page_table(page_table_idx);

    // Temporarily mapping this specific table because it contains VESA video memory
    // TODO: Mark this page table as used in Gfx::setup and initialize it that way
    fill_page_table(0x3f4);

    s_directory->physical_address = (uint32_t)&(s_directory->tables_physical[0]);

    IDT::register_handler(14, [](IDT::interrupt_frame regs) {
        (void)regs;
        uint32_t faulting_address;
        asm volatile("mov %%cr2, %0" :"=r"(faulting_address));

        char msg_buffer[80];
        auto fmt = Formatter::create();
        auto buf = StringBuffer::from_static_array(msg_buffer);
        fmt.format_to(buf, "Page fault at addr 0x%x", faulting_address);

        panic(msg_buffer);
    });

    switch_directory(s_directory);
}

}