#pragma once

#include "Bitset.h"

class Keyboard
{
    friend void register_kb_irq();
public:
    Keyboard();
    ~Keyboard();
    static Keyboard create();

    bool is_key_pressed(uint8_t scancode) const;
private:
    void press_key(uint8_t scancode);
    void release_key(uint8_t scancode);

    uint32_t m_bitset_data[4];
    Bitset m_pressed_keys;
};