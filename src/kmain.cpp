#include <stddef.h>
#include <stdint.h>

#include "gdt.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "gfx.h"
#include "panic.h"
#include "SerialPort.h"
#include "StringBuffer.h"
#include "Formatter.h"
#include "EternalHeap.h"
#include "MemoryManager.h"

#include "Keyboard.h"

Gfx::Color rect_color = {255, 0, 0};

void redraw(uint32_t ticks) {
    Gfx::clear();
    uint16_t x0 = ticks % 100;
    for(uint16_t y=100; y<250; y++) {
        for(uint16_t x=50+x0; x<250+x0; x++)
            Gfx::put_pixel({x, y}, rect_color);
    }
}

// defined in the linker script
extern "C" uint32_t _kernel_end;

extern "C" void kmain()
{
    SerialPort::initialize_ports();

    // switch to 800x600x24 graphics mode
    // mode numbers defined in http://www.monstersoft.com/tutorial1/VESA_intro.html#4
    // vesa functions defined in https://wiki.osdev.org/Getting_VBE_Mode_Info#VESA_Functions
    // by the way, we should do this before initializing descriptors
    // since doing a bios call trashes the descriptor tables
    Gfx::setup(0x0115);

    GDT::setup();
    EternalHeap::setup((void*)&_kernel_end, 1 << 20);

    // Temporarily hardcode 32 mbs of ram. Each page table holds 4M of memory
    // TODO: Get physical memory size from GRUB and use it
    // FIXME: This causes a reset if the number of pages is set too high (Say, 1000)
    MemoryManager::setup(8);

    IDT::setup();
    PIC::setup();
    PIT::setup(60);

    auto keyboard = Keyboard::create();

    SerialPort::com1.puts("Hello\n");
    auto string_buffer = StringBuffer::from_allocation(EternalHeap::allocate<char>(80), 79);

    auto formatter = Formatter::create();
    formatter.format_to(string_buffer, "Format test dec %d hex %x char %c pct %%\n", 42, 42, 'x');
    SerialPort::com1.puts(string_buffer);

    asm volatile ("int $0x3");
    asm volatile ("int $0x4");

    asm volatile("sti");
    // uint32_t* faulty_ptr = (uint32_t*)0xa0000000;
    // auto do_fault = *faulty_ptr;

    uint32_t ticks = 0;
    for(;;) {
        uint32_t new_ticks = PIT::ticks();
        if(new_ticks > ticks) {
            redraw(ticks);
            if(keyboard.is_key_pressed(2))
                rect_color = {255, 0, 0};
            if(keyboard.is_key_pressed(3))
                rect_color = {0, 255, 0};
            if(keyboard.is_key_pressed(4))
                rect_color = {0, 0, 255};

            if(ticks % 10 == 0) {
                SerialPort::com1.puts(".");
            }
            ticks = new_ticks;

            Gfx::sync();
        }
    }
}
