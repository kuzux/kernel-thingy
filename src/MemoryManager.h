#pragma once

#include <stdint.h>

namespace MemoryManager
{
struct PageDirectory;

void setup(uint16_t pages_to_fill); // TODO: Support setting things up at different addresses
PageDirectory* kernel_directory();

void switch_directory(PageDirectory* directory);
// TODO: Support translating from virtual address -> physical
//       (The other way is a bit more complicated, but doable)
}