#pragma once
#include <stddef.h>

#include "panic.h"

template<typename T>
constexpr T&& move(T& arg)
{
    return static_cast<T&&>(arg);
}

template<typename T, size_t Capacity>
class StaticVector
{
public:
    StaticVector()
    {
        m_length = 0;
    }

    size_t length() const { return m_length; }
    size_t capacity() const { return Capacity; }
    
    T& operator[](size_t i)
    {
        ASSERT(i < m_length);
        return m_data[i];
    }

    T& first()
    {
        ASSERT(m_length > 0);
        return m_data[0];
    }

    T& last()
    {
        ASSERT(m_length > 0);
        return m_data[m_length-1];
    }

    void add(T val)
    {
        ASSERT(m_length < Capacity);
        m_data[m_length++] = val;
    }

    void add(T&& val)
    {
        ASSERT(m_length < Capacity);
        m_data[m_length++] = move(val);
    }
private:
    size_t m_length;
    T m_data[Capacity];
};
