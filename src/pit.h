#pragma once
#include <stdint.h>

namespace PIT
{
void setup(uint16_t freq_in_hz);

uint32_t ticks();
}