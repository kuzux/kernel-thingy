#include "gfx.h"
#include "io_instructions.h"

// needed for the int32 function
typedef struct __attribute__ ((packed)) {
    unsigned short di, si, bp, sp, bx, dx, cx, ax;
    unsigned short gs, fs, es, ds, eflags;
} regs16_t;
 
extern "C" void int32(unsigned char intnum, regs16_t *regs);

struct VbeModeInfo {
	uint16_t attributes;		// deprecated, only bit 7 should be of interest to you, and it indicates the mode supports a linear frame buffer.
	uint8_t window_a;			// deprecated
	uint8_t window_b;			// deprecated
	uint16_t granularity;		// deprecated; used while calculating bank numbers
	uint16_t window_size;
	uint16_t segment_a;
	uint16_t segment_b;
	uint32_t win_func_ptr;		// deprecated; used to switch banks from protected mode without returning to real mode
	uint16_t pitch;			// number of bytes per horizontal line
	uint16_t width;			// width in pixels
	uint16_t height;			// height in pixels
	uint8_t w_char;			// unused...
	uint8_t y_char;			// ...
	uint8_t planes;
	uint8_t bpp;			// bits per pixel in this mode
	uint8_t banks;			// deprecated; total number of banks in this mode
	uint8_t memory_model;
	uint8_t bank_size;		// deprecated; size of a bank, almost always 64 KB but may be 16 KB...
	uint8_t image_pages;
	uint8_t reserved0;
 
	uint8_t red_mask;
	uint8_t red_position;
	uint8_t green_mask;
	uint8_t green_position;
	uint8_t blue_mask;
	uint8_t blue_position;
	uint8_t reserved_mask;
	uint8_t reserved_position;
	uint8_t direct_color_attributes;
 
	uint32_t framebuffer;		// physical address of the linear frame buffer; write here to draw to the screen
	uint32_t off_screen_mem_off;
	uint16_t off_screen_mem_size;	// size of memory in the framebuffer but not being displayed on the screen
	uint8_t reserved1[206];
} __attribute__ ((packed));

static VbeModeInfo mode_info;

static struct {
    uint8_t r, g, b, total;
} offsets;

// Let's reserve the largest possible size for the back buffer
static uint8_t back_buffer[1280*1024*4];

namespace Gfx
{
void setup(uint16_t mode) 
{
    regs16_t regs;
    
    regs.ax = 0x4f02;
    regs.bx = mode;
    regs.bx |= 0x4000;
    int32(0x10, &regs);

    regs.ax = 0x4f01;
    regs.cx = mode;
    int32(0x10, &regs);
    VbeModeInfo* info_ptr = (VbeModeInfo*)(regs.es << 16 | regs.di);
    mode_info = *info_ptr;

    offsets.r = mode_info.red_position / 8;
    offsets.g = mode_info.green_position / 8;
    offsets.b = mode_info.blue_position / 8;
    offsets.total = mode_info.bpp / 8;
}

void put_pixel(Coords coords, Color color)
{
    uint8_t* pixel = back_buffer + coords.y * mode_info.pitch + coords.x * offsets.total;
    pixel[offsets.r] = color.r;
    pixel[offsets.g] = color.g;
    pixel[offsets.b] = color.b;
}

void clear()
{
    uint8_t* base = back_buffer;
    uint32_t len = mode_info.pitch * mode_info.height;

    for(uint32_t i=0; i<len; i++)
        base[i] = 0;
}

uint16_t width() { return mode_info.width; }

uint16_t height() { return mode_info.height; }

void sync() {
    uint8_t status = 0x0;
	do {
		status = inb(0x03da);
	} while ((status & 0x08) == 0);

	uint8_t* front_buffer = (uint8_t*)mode_info.framebuffer;
	for(uint32_t i=0; i<mode_info.height*mode_info.pitch; i++)
		front_buffer[i] = back_buffer[i];
}

}