#pragma once

#include <stddef.h>
#include <stdint.h>

inline void* operator new(size_t, void* p) { return p; }
namespace EternalHeap
{

void setup(void* start, uint32_t size);
uint8_t* allocate_bytes(uint32_t size, uint32_t align_to);

template<typename T>
T* allocate(uint32_t count, uint32_t align_to = 0x0)
{
    return (T*)allocate_bytes(count * sizeof(T), align_to);
}

template<typename T, typename...Args>
T* create(Args... args)
{
    void* ptr = allocate<T>(1);
    return new (ptr) T(args...);
}

}
