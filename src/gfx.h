#pragma once
#include <stdint.h>

namespace Gfx
{
    struct Coords
    {
        uint16_t x, y;
    };

    struct Color
    {
        uint8_t r, g, b;
    };

    void setup(uint16_t mode);
    void put_pixel(Coords coords, Color color);

    void clear();
    void sync();

    uint16_t width();
    uint16_t height();
}