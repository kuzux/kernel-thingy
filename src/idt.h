#pragma once
#include <stdint.h>

namespace IDT
{
struct interrupt_frame
{
    uint32_t ds;                                        // Data segment selector
    uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;    // Pushed by pusha.
    uint32_t int_no, err_code;                          // Interrupt number and error code (if applicable)
    uint32_t eip, cs, eflags, useresp, ss;              // Pushed by the processor automatically.
};

typedef void (*interrupt_handler)(interrupt_frame);

void setup();
void register_handler(uint8_t interrupt_no, interrupt_handler handler);
}