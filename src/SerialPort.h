#pragma once

#include <stdint.h>
#include "StringBuffer.h"

class SerialPort
{
public:
    static SerialPort com1;

    void write(uint8_t value);
    void puts(const char* string);
    void puts(StringBuffer const& buffer);

    static void initialize_ports();
private:
    bool can_write() const;
    SerialPort() { }
    void initialize(uint16_t port);

    uint16_t m_port { 0x00 };
    bool m_works { false };
};