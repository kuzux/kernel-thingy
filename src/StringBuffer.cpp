#include "StringBuffer.h"
#include "panic.h"

StringBuffer::~StringBuffer()
{
}

uint32_t StringBuffer::length() const
{
    for(uint32_t i=0; i<m_capacity; i++) {
        if(m_buffer[i] == 0) return i;
    }

    return m_capacity;
}

char* StringBuffer::unsafe_end_pointer()
{
    auto len = length();
    return m_buffer + len;
}

void StringBuffer::append(char ch)
{
    auto len = length();
    ASSERT(len >= m_capacity - 1);

    m_buffer[len] = ch;
    m_buffer[len+1] = 0;
}

void StringBuffer::clear()
{
    m_buffer[0] = 0;
}
