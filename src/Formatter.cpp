#include "Formatter.h"

static uint32_t format_decimal(char* buf, uint32_t max_len, int32_t n)
{
    if(n == 0) {
        if(max_len == 0) return 0;
        buf[0] = '0';
        return 1;
    }

    bool seen_nonzero = false;
    uint8_t output_len = 0;

    int32_t curr = n;
    if(curr < 0) {
        if(max_len == 0) return 0;
        buf[0] = '-';
        if(max_len == 1) return 1;

        output_len++;
        curr = -n;
    }

    char reverse_num[32];
    uint8_t reverse_idx = 0;

    while(curr) {
        reverse_num[reverse_idx++] = curr % 10;
        curr /= 10;
    }

    for(uint8_t i=0; i<reverse_idx; i++) {
        buf[output_len] = '0'+reverse_num[reverse_idx-i-1];
        output_len++;
    }

    return output_len;
}

static uint32_t format_hex(char* buf, uint32_t max_len, uint32_t n)
{
    if(n == 0) {
        if(max_len == 0) return 0;
        buf[0] = '0';
        return 1;
    }

    bool seen_nonzero = false;
    uint8_t output_len = 0;

    for(int8_t digit_offset = 28; digit_offset >= 0; digit_offset -= 4) {
        uint32_t digit_mask = 0xF << digit_offset;
        uint8_t digit = (n & digit_mask) >> digit_offset;

        if(digit == 0 && !seen_nonzero) continue;
        if(digit != 0) seen_nonzero = true;

        if(digit < 0xA)
            buf[output_len++] = '0'+digit;
        else
            buf[output_len++] = 'a'+(digit-0xA);

        if(output_len == max_len) break;
    }

    return output_len;
}

uint32_t Formatter::vformat_to(StringBuffer& buffer, char const* format, va_list args)
{
    char* raw_buffer = buffer.unsafe_end_pointer();

    uint32_t remaining_capacity = buffer.capacity() - buffer.length();
    uint32_t output_length = 0;

    uint32_t max_format_len = 65536;
    for(uint32_t i=0; i < max_format_len; i++)
    {
        if(!format[i]) break;

        if(format[i] == '%') {
            char next = format[i+1];

            int next_arg = 0;

            switch(next) {
            case 'd':
                next_arg = va_arg(args, int);
                output_length += format_decimal(raw_buffer+output_length, remaining_capacity - output_length, next_arg);
                i++;
                break;
            case 'x':
                next_arg = va_arg(args, int);
                output_length += format_hex(raw_buffer+output_length, remaining_capacity - output_length, next_arg);
                i++;
                break;
            case 'c':
                next_arg = va_arg(args, int);
                if(output_length < remaining_capacity)
                    raw_buffer[output_length++] = (char)next_arg;
                i++;
                break;
            case '%':
                if(output_length < remaining_capacity)
                    raw_buffer[output_length++] = '%';
                i++;
                break;
            default:
                break;
            }
        } else {
            if(output_length < remaining_capacity)
                raw_buffer[output_length++] = format[i];
        }
    }

    if(output_length < remaining_capacity)
        raw_buffer[output_length] = 0;
    return output_length;
}
