#include "panic.h"
#include "EternalHeap.h"

namespace EternalHeap
{

static uint32_t s_start;
static uint32_t s_end;

static uint32_t s_curr_addr;

void setup(void* start, uint32_t size)
{
    s_start = (uint32_t)start;
    s_end = s_start + size;
    ASSERT(s_start < s_end);

    s_curr_addr = s_start;
}

uint8_t* allocate_bytes(uint32_t size, uint32_t align_to)
{
    uint32_t return_address = s_curr_addr;
    if(align_to) {
        auto alignment_mask = 0xFFFFFFFF - (align_to - 1);

        return_address &= alignment_mask;

        if(return_address != s_curr_addr)
            return_address += align_to;
    }
    s_curr_addr = return_address + size;
    ASSERT(s_curr_addr < s_end);

    return (uint8_t*)return_address;
}

};