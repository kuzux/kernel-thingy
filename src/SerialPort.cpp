#include "SerialPort.h"
#include "io_instructions.h"

void SerialPort::initialize(uint16_t port)
{
    m_port = port;

    outb(port + 1, 0x00);    // Disable all interrupts
    outb(port + 3, 0x80);    // Enable DLAB (set baud rate divisor)
    outb(port + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
    outb(port + 1, 0x00);    //                  (hi byte)
    outb(port + 3, 0x03);    // 8 bits, no parity, one stop bit
    outb(port + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
    outb(port + 4, 0x0B);    // IRQs enabled, RTS/DSR set
    outb(port + 4, 0x1E);    // Set in loopback mode, test the serial chip
    outb(port + 0, 0xAE);    // Test serial chip (send byte 0xAE and check if serial returns same byte)

    // Check if serial is faulty (i.e: not same byte as sent)
    if(inb(port + 0) != 0xAE)
        return;

    // If serial is not faulty set it in normal operation mode
    // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
    outb(port + 4, 0x0F);
    m_works = true;
    return;
}

SerialPort SerialPort::com1;
void SerialPort::initialize_ports()
{
    SerialPort::com1.initialize(0x3f8);
}

bool SerialPort::can_write() const
{
    return inb(m_port + 5) & 0x20;
}

void SerialPort::write(uint8_t value)
{
    if(!m_works) return;

    while(!can_write());
    outb(m_port, value);
}

void SerialPort::puts(const char* str)
{
    char c;
    while((c = *str)) {
        write(c);
        str++;
    }
}

void SerialPort::puts(StringBuffer const& buffer)
{
    auto len = buffer.length();
    auto str = buffer.start_pointer();

    for(uint32_t i=0; i<len; i++) {
        write(str[i]);
    }
}