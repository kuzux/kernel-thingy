#include "Keyboard.h"
#include "EternalHeap.h"
#include "idt.h"
#include "StaticVector.h"
#include "ByteOperations.h"
#include "io_instructions.h"

static StaticVector<Keyboard, 128> s_kbs;
static bool s_registered_irq { false };

constexpr uint8_t IRQ1 = 33;

void register_kb_irq()
{
    IDT::register_handler(IRQ1, [](IDT::interrupt_frame regs){
        (void)regs;
        uint8_t scancode = inb(0x60);

        // TODO: Implement range based for loops
        for(int i=0; i<s_kbs.length(); i++) {
            auto& kb = s_kbs[i];
            if(scancode & 0x80)
                kb.release_key(scancode & ~0x80);
            else
                kb.press_key(scancode);
        }
    });
}

Keyboard Keyboard::create()
{
    s_kbs.add(*EternalHeap::create<Keyboard>());
    if(!s_registered_irq) register_kb_irq();
    return s_kbs.last();
}

bool Keyboard::is_key_pressed(uint8_t scancode) const
{
    return m_pressed_keys.test(scancode);
}

Keyboard::Keyboard()
    : m_pressed_keys(m_bitset_data, 128) { }
Keyboard::~Keyboard() { }

void Keyboard::press_key(uint8_t scancode)
{
    m_pressed_keys.set(scancode);
}

void Keyboard::release_key(uint8_t scancode)
{
    m_pressed_keys.clear(scancode);
}