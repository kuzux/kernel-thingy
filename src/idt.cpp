#include <stdint.h>
#include "idt.h"
#include "pic.h"
#include "SerialPort.h"

/* Defines an IDT entry */
struct idt_entry
{
    unsigned short base_lo;
    unsigned short sel;        /* Our kernel segment goes here! */
    unsigned char always0;     /* This will ALWAYS be set to 0! */
    unsigned char flags;       /* Set using the above table! */
    unsigned short base_hi;
} __attribute__((packed));

struct idt_ptr
{
    unsigned short limit;
    unsigned int base;
} __attribute__((packed));

/* Declare an IDT of 256 entries. Although we will only use the
*  first 32 entries in this tutorial, the rest exists as a bit
*  of a trap. If any undefined IDT entry is hit, it normally
*  will cause an "Unhandled Interrupt" exception. Any descriptor
*  for which the 'presence' bit is cleared (0) will generate an
*  "Unhandled Interrupt" exception */
struct idt_entry idt[256];
extern "C" struct idt_ptr _idtp;

/* This exists in 'start.asm', and is used to load our IDT */
extern "C" void _idt_flush();

void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags)
{
    idt[num].base_lo = (base & 0xFFFF);
    idt[num].base_hi = (base >> 16) & 0xFFFF;
    idt[num].always0 = 0;

    idt[num].sel = sel;
    idt[num].flags = flags;
}

extern "C" void _isr0();
extern "C" void _isr1();
extern "C" void _isr2();
extern "C" void _isr3();
extern "C" void _isr4();
extern "C" void _isr5();
extern "C" void _isr6();
extern "C" void _isr7();
extern "C" void _isr8();
extern "C" void _isr9();
extern "C" void _isr10();
extern "C" void _isr11();
extern "C" void _isr12();
extern "C" void _isr13();
extern "C" void _isr14();
extern "C" void _isr15();
extern "C" void _isr16();
extern "C" void _isr17();
extern "C" void _isr18();
extern "C" void _isr19();
extern "C" void _isr20();
extern "C" void _isr21();
extern "C" void _isr22();
extern "C" void _isr23();
extern "C" void _isr24();
extern "C" void _isr25();
extern "C" void _isr26();
extern "C" void _isr27();
extern "C" void _isr28();
extern "C" void _isr29();
extern "C" void _isr30();
extern "C" void _isr31();

extern "C" void _irq0();
extern "C" void _irq1();
extern "C" void _irq2();
extern "C" void _irq3();
extern "C" void _irq4();
extern "C" void _irq5();
extern "C" void _irq6();
extern "C" void _irq7();
extern "C" void _irq8();
extern "C" void _irq9();
extern "C" void _irq10();
extern "C" void _irq11();
extern "C" void _irq12();
extern "C" void _irq13();
extern "C" void _irq14();
extern "C" void _irq15();

IDT::interrupt_handler irq_handlers[256];

extern "C" void isr_handler(IDT::interrupt_frame regs)
{
    if(irq_handlers[regs.int_no] != nullptr) {
        irq_handlers[regs.int_no](regs);
        return;
    }

    SerialPort::com1.puts("got unhandled interrupt ");
    SerialPort::com1.write('0'+regs.int_no/10);
    SerialPort::com1.write('0'+regs.int_no%10);
    SerialPort::com1.write('\n');
}

extern "C" void irq_handler(IDT::interrupt_frame regs)
{
    if(irq_handlers[regs.int_no] != nullptr)
        irq_handlers[regs.int_no](regs);

    PIC::send_end_of_interrupt(regs.int_no);
}

namespace IDT
{
void setup() 
{
    _idtp.base = (uint32_t)&idt;
    _idtp.limit = (sizeof (struct idt_entry) * 256) - 1;

    idt_set_gate( 0, (uint32_t)_isr0,  0x08, 0x8e);
    idt_set_gate( 1, (uint32_t)_isr1,  0x08, 0x8e);
    idt_set_gate( 2, (uint32_t)_isr2,  0x08, 0x8e);
    idt_set_gate( 3, (uint32_t)_isr3,  0x08, 0x8e);
    idt_set_gate( 4, (uint32_t)_isr4,  0x08, 0x8e);
    idt_set_gate( 5, (uint32_t)_isr5,  0x08, 0x8e);
    idt_set_gate( 6, (uint32_t)_isr6,  0x08, 0x8e);
    idt_set_gate( 7, (uint32_t)_isr7,  0x08, 0x8e);
    idt_set_gate( 8, (uint32_t)_isr8,  0x08, 0x8e);
    idt_set_gate( 9, (uint32_t)_isr9,  0x08, 0x8e);
    idt_set_gate(10, (uint32_t)_isr10, 0x08, 0x8e);
    idt_set_gate(11, (uint32_t)_isr11, 0x08, 0x8e);
    idt_set_gate(12, (uint32_t)_isr12, 0x08, 0x8e);
    idt_set_gate(13, (uint32_t)_isr13, 0x08, 0x8e);
    idt_set_gate(14, (uint32_t)_isr14, 0x08, 0x8e);
    idt_set_gate(15, (uint32_t)_isr15, 0x08, 0x8e);
    idt_set_gate(16, (uint32_t)_isr16, 0x08, 0x8e);
    idt_set_gate(17, (uint32_t)_isr17, 0x08, 0x8e);
    idt_set_gate(18, (uint32_t)_isr18, 0x08, 0x8e);
    idt_set_gate(19, (uint32_t)_isr19, 0x08, 0x8e);
    idt_set_gate(20, (uint32_t)_isr20, 0x08, 0x8e);
    idt_set_gate(21, (uint32_t)_isr21, 0x08, 0x8e);
    idt_set_gate(22, (uint32_t)_isr22, 0x08, 0x8e);
    idt_set_gate(23, (uint32_t)_isr23, 0x08, 0x8e);
    idt_set_gate(24, (uint32_t)_isr24, 0x08, 0x8e);
    idt_set_gate(25, (uint32_t)_isr25, 0x08, 0x8e);
    idt_set_gate(26, (uint32_t)_isr26, 0x08, 0x8e);
    idt_set_gate(27, (uint32_t)_isr27, 0x08, 0x8e);
    idt_set_gate(28, (uint32_t)_isr28, 0x08, 0x8e);
    idt_set_gate(29, (uint32_t)_isr29, 0x08, 0x8e);
    idt_set_gate(30, (uint32_t)_isr30, 0x08, 0x8e);
    idt_set_gate(31, (uint32_t)_isr31, 0x08, 0x8e);
    idt_set_gate(32, (uint32_t)_irq0,  0x08, 0x8e);
    idt_set_gate(33, (uint32_t)_irq1,  0x08, 0x8e);
    idt_set_gate(34, (uint32_t)_irq2,  0x08, 0x8e);
    idt_set_gate(35, (uint32_t)_irq3,  0x08, 0x8e);
    idt_set_gate(36, (uint32_t)_irq4,  0x08, 0x8e);
    idt_set_gate(37, (uint32_t)_irq5,  0x08, 0x8e);
    idt_set_gate(38, (uint32_t)_irq6,  0x08, 0x8e);
    idt_set_gate(39, (uint32_t)_irq7,  0x08, 0x8e);
    idt_set_gate(40, (uint32_t)_irq8,  0x08, 0x8e);
    idt_set_gate(41, (uint32_t)_irq9,  0x08, 0x8e);
    idt_set_gate(42, (uint32_t)_irq10, 0x08, 0x8e);
    idt_set_gate(43, (uint32_t)_irq11, 0x08, 0x8e);
    idt_set_gate(44, (uint32_t)_irq12, 0x08, 0x8e);
    idt_set_gate(45, (uint32_t)_irq13, 0x08, 0x8e);
    idt_set_gate(46, (uint32_t)_irq14, 0x08, 0x8e);
    idt_set_gate(47, (uint32_t)_irq15, 0x08, 0x8e);

    _idt_flush();
}

void register_handler(uint8_t interrupt_no, interrupt_handler handler)
{
    irq_handlers[interrupt_no] = handler;
}
}