#pragma once

#include <stdint.h>
#include <stdarg.h>
#include "StringBuffer.h"

class Formatter
{
public:
    static Formatter create() { return Formatter {}; }

    template<uint32_t S>
    uint32_t format_to(StringBuffer& buffer, char const (&format)[S], ...)
    {
        va_list args;
        va_start(args, format);
        auto res = vformat_to(buffer, format, args);
        va_end(args);

        return res;
    }
private:
    uint32_t vformat_to(StringBuffer& buffer, char const* format, va_list args);
};
