#pragma once

#include <stdint.h>

namespace ByteOperations
{

inline void fill(void* dest, uint8_t byte, uint32_t length)
{
    auto* dest_bytes = (uint8_t*)dest;
    for(uint32_t i=0; i<length; i++) dest_bytes[i] = byte;
}

inline void clear(void* dest, uint32_t length)
{
    fill(dest, 0, length);
}

inline void copy(void* dest, void* src, uint32_t length)
{
    auto* dest_bytes = (uint8_t*)dest;
    auto* src_bytes = (uint8_t*)src;
    for(uint32_t i=0; i<length; i++) dest_bytes[i] = src_bytes[i];
}

}