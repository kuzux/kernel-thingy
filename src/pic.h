#pragma once
#include <stdint.h>

namespace PIC
{
void send_end_of_interrupt(uint8_t interrupt_no);
void setup();

uint32_t ticks();
}
