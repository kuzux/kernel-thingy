#include "pit.h"
#include "idt.h"
#include "io_instructions.h"

#define IRQ0 32

namespace PIT
{
static void configure_frequency(uint16_t freq_in_hz)
{
    // The value we send to the PIT is the value to divide it's input clock
    // (1193180 Hz) by, to get our required frequency. Important to note is
    // that the divisor must be small enough to fit into 16-bits.
    uint16_t divisor = 1193180 / freq_in_hz;

    // Send the command byte.
    outb(0x43, 0x36);

    // Divisor has to be sent byte-wise, so split here into upper/lower bytes.
    uint8_t lo = (uint8_t)(divisor & 0xFF);
    uint8_t hi = (uint8_t)((divisor>>8) & 0xFF);

    // Send the frequency divisor.
    outb(0x40, lo);
    outb(0x40, hi);
}

static uint32_t ticks_count;

void setup(uint16_t freq_in_hz)
{
    ticks_count = 0;
    IDT::register_handler(IRQ0, [](IDT::interrupt_frame regs) {
        (void)regs;
        ticks_count++;
    });
    configure_frequency(freq_in_hz);
}

uint32_t ticks() { return ticks_count; }

}